<?php
/*
Template Name: [ STOCKISTS ]
*/

get_header('blog'); ?>
<section class="page_head">
<div class="container">
	<h1>Blog</h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et <br> dolore magna aliqua.</p>
</div>
</section>
<section class="result_sec"><?php
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$newsArgs = array(
		'post_type' => 'blog', 
		'post_status' => array('publish'), 
		'posts_per_page' => get_option( 'posts_per_page' ),
		'paged' => $paged
	);
	$totalpages = '';
	$newsQuery = new WP_Query( $newsArgs );
	if ( $newsQuery->have_posts() ) 
	{	?>
	
		<section class="home_latest">
			<div class="container">
				<div class="row"><?php
					while ( $newsQuery->have_posts() ) 
					{
						$newsQuery->the_post();	?>
						<div class="col-md-6">
							<div class="block home_blog">
								<figure><?php 
									if ( has_post_thumbnail() ) : ?>
										<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
											<?php the_post_thumbnail(); ?>
										</a><?php 
									endif; ?>
								</figure>
								<div class="title"><?= the_title(); ?></div>
								<div class="date">Blog – <?php echo get_the_date( 'd/m/Y', $post->ID );?></div>
							</div>
						</div><?php
					}	?>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-12 next_prev">
						<!-- <p>Showing Page 1 of 5</p> -->
						<?php wp_pagenavi(array('query' => $newsQuery)); ?>
						<div class="clearfix"></div>
						<!-- <div class="btns">
							<a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i> Previous</a> | <a href="#">Next <i class="fa fa-angle-right" aria-hidden="true"></i></a>
						</div> -->
					</div>
				</div>
			</div>
		</section><?php
	}
	else
	{
		get_template_part( 'template-parts/blog/content', 'none' );
	}	?>
</section>


<?php get_footer();
