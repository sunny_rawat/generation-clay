<?php

/*

Template Name: Results

*/



get_header('blog'); ?>

<section class="page_head">

<div class="container">

	<h1>Results</h1>

<?php the_field('result_header_heading'); ?>

</div>

</section>





<?php

	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	$newsArgs = array(

		'post_type' => 'results', 

		'post_status' => array('publish'), 

		'posts_per_page' => get_option( 'posts_per_page' ),

		'paged' => $paged

	);

	$totalpages = '';

	$newsQuery = new WP_Query( $newsArgs );

	if ( $newsQuery->have_posts() ) 

	{	?>



<section class="result_sec">

	<div class="container">

		<div class="row">



			<?php while ( $newsQuery->have_posts() ) 

					{

						$newsQuery->the_post();	
					$external_url =	get_field('external_url');

						?>

		<div class="col-md-3 col-sm-4 col-xs-6 blockup">

			<div class="block">

				<figure>

					<?php 

					if ( has_post_thumbnail() ) : 

						the_post_thumbnail();  

					endif; ?>

				</figure>

				<div class="text_block">

					<h4><a href="<?php echo $external_url; ?>" target="_blank"><?php the_field('result_username'); ?></a></h4>
					 
					<?php the_content(); ?>

				</div>

			</div>

		</div>



		<?php } wp_reset_query(); 	

		if (  $newsQuery->max_num_pages > 1 ):
			 echo '<div class="results_loadmore">More Results</div>'; 
			endif;

		?>

 

	</div>

 
	<div class="row">

	<div class="col-md-12 page_bottom_text">

		<?php the_field('result_footer_heading'); ?>

	</div>

</div>





	</div>

</section>





<section class="foot_banner">
<a href="<?php echo site_url('communities'); ?>">
	<img src="<?php the_field('result_page_banner'); ?>">

	<h2><?php the_field('result_page_banner_heading'); ?></h2>
</a>
</section>



<?php

	}

	else

	{

		get_template_part( 'template-parts/blog/content', 'none' );

	}	?>



<?php get_footer();

