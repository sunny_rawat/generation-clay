<?php

/*

Template Name: About

*/



get_header('blog'); ?>

<section class="about_banner">
	<img src="<?php the_field('banner_image'); ?>" class="cover_pic">
	<div class="text_in">
		<?php the_field('banner_title'); ?>
	</div>
</section>

<section class="home_sec1">
	<div class="container">
		<div class="text_in">
		<h2><?php echo get_field('section_2_title'); ?></h2>
		<?php echo get_field('section_2_content'); ?>
	</div>
	</div>
</section>

<section class="blurb_sec">
	<div class="container">
		<?php  if( have_rows('section_3_contents') ): 
		while ( have_rows('section_3_contents') ) : the_row(); ?>
		<div class="col-md-3">
			<div class="block">
				<figure>
					<img src="<?php echo get_sub_field('section_3_image'); ?>">
				</figure>
				<div class="text_block">
					<h3><?php echo get_sub_field('section_3_title'); ?></h3>
					<p><?php echo get_sub_field('section_3_content'); ?></p>
				</div>
			</div>
		</div>

		<?php endwhile;
		  endif; ?>  
	</div>
</section>


<section class="about_imgsec" style="background-image: url(<?php echo get_field('section_4_banner'); ?>)">
	<div class="container">
		<div class="small_img"><img src="<?php echo get_field('section_4_image'); ?>"></div>
	</div>
</section>


<section class="partner_sec">
	<div class="container">
		<h2><?php echo get_field('our_partner_title'); ?></h2>
		<ul>
 		<?php  if( have_rows('our_partners') ): 
		while ( have_rows('our_partners') ) : the_row(); ?>
		<li><a href="<?php echo get_sub_field('partner_logo'); ?>"><img src="<?php echo get_sub_field('partner_logo'); ?>"></a></li>
		 <?php endwhile;
		  endif; ?> 
			
		</ul>
	</div>
</section>


<section class="faq_sec">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<h2><?php echo get_field('faq_title'); ?></h2>

				<ul class="faqul">

					<?php  if( have_rows('faq_content') ): 
		while ( have_rows('faq_content') ) : the_row(); ?>
		<li>
						<h4><?php echo get_sub_field('title_faq'); ?></h4>
						<div class="text_sec"><?php echo get_sub_field('contents_faq'); ?></div>
					</li>

		 <?php endwhile;
		  endif; ?> 
 
					 
				</ul>
			</div>
			<div class="col-md-3">

				<?php  if( have_rows('about_contacts') ): 
		while ( have_rows('about_contacts') ) : the_row(); ?>
		<div class="side_sec">
					<h3><?php echo get_sub_field('title_right'); ?></h3>
					<?php echo get_sub_field('contents_right'); ?>
				</div>
		 <?php endwhile;
		  endif; ?> 
 
			</div>
		</div>
	</div>
</section>
<?php get_footer();