<?php

/*

Template Name: [ BLOG ]

*/



get_header('blog'); ?>

<section class="page_head">

<div class="container">
	<h1><?php _e( 'WORLD OF GEN CLAY<sup>TM</sup>', 'twentyseventeen' ); ?> <?php //the_title(); ?></h1>
	<?php the_content(); ?>
</div>

</section>

<section class="result_sec"><?php

	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	$newsArgs = array(

		'post_type' => 'blog', 

		'post_status' => array('publish'), 

		'posts_per_page' => get_option( 'posts_per_page' ),

		'paged' => $paged

	);

	$totalpages = '';

	$newsQuery = new WP_Query( $newsArgs );

	if ( $newsQuery->have_posts() ) 

	{	?>

	
		<section class="home_latest">

			<div class="container">

				<div class="row ajaxappending"><?php

					while ( $newsQuery->have_posts() ) 

					{

						$newsQuery->the_post();	?>

						<div class="col-md-6">

							<div class="block home_blog">

								<?php if ( has_post_thumbnail() ) : ?>
										<figure><?php the_post_thumbnail(); ?><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"></a></figure>
 									<?php endif; ?>
								<div class="title"> <a href="<?php the_permalink(); ?>"><?= the_title(); ?></a></div>

								<div class="date"><?php _e( 'Blog – ', 'twentyseventeen' ); ?> <?php echo get_the_date('m/d/Y');?></div>

							</div>

						</div><?php

					}	?>

						<?php
							
							if (  $newsQuery->max_num_pages > 1 ) {
								echo '<div class="blog_loadmore">More Blogs</div>'; 
							}
						 ?>
				</div>

			</div>
 

		</section><?php

	}

	else

	{

		get_template_part( 'template-parts/blog/content', 'none' );

	}	?>

</section>
<?php get_footer();

