<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header('blog'); ?>


<script type="text/javascript">
(function() {
    window.PinIt = window.PinIt || { loaded:false };
    if (window.PinIt.loaded) return;
    window.PinIt.loaded = true;
    function async_load(){
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.async = true;
        s.src = "http://assets.pinterest.com/js/pinit.js";
        var x = document.getElementsByTagName("script")[0];
        x.parentNode.insertBefore(s, x);
    }
    if (window.attachEvent)
        window.attachEvent("onload", async_load);
    else
        window.addEventListener("load", async_load, false);
})();
</script>

<?php while ( have_posts() ) : the_post(); 
	$cat = wp_get_post_terms(get_the_ID(),'community-categories');


	$external_url = get_field('community_external_url');
?>
<section class="blog_post">
	<div class="container">
		<div class="date1"><?php _e( 'Community', 'twentyseventeen' ); ?>  |  <?php echo $cat[0]->name; ?>  |  <?php the_date('m/d/Y'); ?></div>
		<div class="banner_img">
			<img src="<?php echo get_the_post_thumbnail_url(); ?>">
		</div>
		<div class="post_in">
			<h1><?php the_title(); ?></h1>
			<?php the_content(); ?>


<?php if($external_url){ ?>
<a href="<?php echo $external_url; ?>" target="_blank" class="read_full_art"><?php _e( 'Read the full article on the website', 'twentyseventeen' ); ?></a>
<?php } ?>



<div class="share_post">

<?php $title = get_the_title();
$url= get_the_permalink();
$summary=urlencode(get_the_content());

$attachment_idpost = get_post_thumbnail_id(get_the_ID());
$imagepost = wp_get_attachment_image_src( $attachment_idpost, 'thumbnail' );
 ?>
 

	<?php _e( 'Share:', 'twentyseventeen' ); ?> 

<a id="ref_pr" href="http://pinterest.com/pin/create/bookmarklet/?media=<?php echo $imagepost[0];?>&amp;
url=<?php echo $url; ?>&amp;
is_video=false&amp;description=<?php echo $title;?>"
onclick="javascript:window.open(this.href, '_blank', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');return false;"><i class="fa fa-pinterest" aria-hidden="true"></i></a>

 <a class="fb-share" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>&picture=<?php echo $imagepost[0];?>" target="_blank">
    <i class="fa fa-facebook" aria-hidden="true"></i>
</a>

<a class="tw-share" target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo $url; ?>&amp;text=<?php echo $title;?>&amp;via=<?php bloginfo('name'); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>


 

</div>

		</div>
	</div>
</section>

<section class="foot_banner">
	<img src="<?php the_field('banner_image'); ?>">
	<h2><?php the_field('banner_title'); ?></h2>
</section>

<?php endwhile;
get_footer();
