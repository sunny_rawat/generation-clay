<?php

/**

* The header for our theme

*

* This is the template that displays all of the <head> section and everything up until <div id="content">

*

* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

*

* @package WordPress

* @subpackage Twenty_Seventeen

* @since 1.0

* @version 1.0

*/



?>

<!DOCTYPE html>

<html <?php language_attributes(); ?> class="no-js no-svg" id="top">

<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.typekit.net/sgh2ajj.css">
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<header>

<section class="topbar text-center">

<?php //if ( isset($_COOKIE['clay-header-text']) ) :  ?>
  <div class="container">
<?php the_field('header_text','option'); ?>
  </div>
 <?php //endif; ?>

</section>
<div class="cart_overlay"></div>
	<div class="cart_popup">
		<a href="javascript:;" class="close1"></a>
		<h3 class="shopingbagtotal"><?php _e( 'Shopping Bag ', 'twentyseventeen' ); ?>(<?php echo WC()->cart->get_cart_contents_count(); ?>)</h3>
		<!--<h4>Your bag is currently empty... Weird!</h4>
		<h4>You can help you fill it up here</h4>-->

		 <div class="woo_mini_sidebar_cart">
		 <?php woocommerce_get_template( 'cart/mini-cart.php'); ?>
		 </div>

		<div class="bottom_sec">
		<ul class="ut_list">
		<li><img src="<?php echo CHILD_DIR; ?>/img/home_icon1.png"><div class="clearfix"></div> <?php _e( 'Free<br>Shipping', 'twentyseventeen' ); ?></li>
		<li><img src="<?php echo CHILD_DIR; ?>/img/home_icon1.png"><div class="clearfix"></div> <?php _e( '30 Day<br>Returns', 'twentyseventeen' ); ?></li>
		<li><img src="<?php echo CHILD_DIR; ?>/img/home_icon1.png"><div class="clearfix"></div> <?php _e( '24/7 Cost<br>Supp', 'twentyseventeen' ); ?></li>
		</ul>

			<table cellpadding="0" border="0" cellspacing="0">
				<!--<tr><td>Subtotal</td><td>$00.00</td></tr>-->
				<tr><td><a href="<?php echo get_permalink(woocommerce_get_page_id('shop')); ?>"><?php _e( 'Continue Shopping', 'twentyseventeen' ); ?></a></td><td><a href="<?php echo site_url('checkout'); ?>" class="cart_checkout_btn"><?php _e( 'Checkout', 'twentyseventeen' ); ?></a></td></tr>
			</table>
		</div>
	</div>
<section class="navbar">
<div class="mob_menu">
	<div class="mob_menuin">
  	<?php wp_nav_menu( array(
		'theme_location' => 'top',
		'menu_id'        => 'top-menu-mobile',
		'container'      => 'ul',
		'menu_class'     => 'ul1',
		) ); ?> 
	 
	<?php if( have_rows('social_icons','option') ) {	?>
	<ul class="ul4">
			<?php while ( have_rows('social_icons','option') ) { 
					the_row(); ?>
		<li><a href="<?php the_sub_field("url_social"); ?>" target="_blank"><i class="fa <?php the_sub_field("icon_social"); ?>" aria-hidden="true"></i></a></li>
		<?php } ?>
	</ul>
	<?php } ?>
	<?php wp_nav_menu( array(
					'menu_class'     => 'ul3',
					'theme_location' => 'footer-information',
					'container'      => 'ul',
				) ); ?>
	</div></div>

  <div class="container-fluid">

  <a href="<?php echo site_url(); ?>" class="brand_logo">
<span class="desktop_logo">
  	<img src="<?php the_field('logo_white','option'); ?>" class="img1">  

  	<img src="<?php the_field('logo_black','option'); ?>" class="img2">
</span>
<span class="mobile_logo">
  	<img src="<?php the_field('mobile_logo_white','option'); ?>" class="img1">  

  	<img src="<?php the_field('mobile_logo_black','option'); ?>" class="img2">
</span>
  </a>



<?php wp_nav_menu( array(
		'theme_location' => 'top',
		'menu_id'        => 'top-menu',
		'container'      => 'ul',
		'menu_class'     => 'main_menu',
	) ); ?>

 <div class="header_right">

 	<?php if( have_rows('social_icons','option') ) {	?>
	<div class="media_icons">
			<?php while ( have_rows('social_icons','option') ) { 
					the_row(); ?>
		<a href="<?php the_sub_field("url_social"); ?>" target="_blank"><i class="fa <?php the_sub_field("icon_social"); ?>" aria-hidden="true"></i></a>
		<?php } ?>
	</div>
	<?php } ?>



	<a href="javascript:void(0);" class="cart_icon">

		<span class="cart_no"><?php echo WC()->cart->get_cart_contents_count(); ?></span>

	</a>

	<a href="javascript:void(0);" class="mob_menu_icon"></a>

</div>

  </div>

</section>

</header>

<section class="home_banner"><?php

	if( have_rows('slides','option') )

	{	?>

 		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

			<ol class="carousel-indicators"><?php

			

				$i = 0;

				while ( have_rows('slides','option') )

				{ 

					the_row(); ?>

					<li data-target="#carousel-example-generic" data-slide-to="<?php _e($i); ?>" class="<?php if($i==0){ echo "active"; } ?>"></li><?php

					$i++;

				} ?>

			</ol>

			<div class="carousel-inner" role="listbox"><?php

				$j = 0;

				while ( have_rows('slides','option') )

				{ 

					the_row();

					 if ( wp_is_mobile() ) {
					 	$slideimage = get_sub_field("mobile_image");
					 } else {
					 	$slideimage = get_sub_field("image");
					 }


					 ?>

					<div class="item <?php if($j==0){ echo "active"; } ?>" style="background-image: url(<?php echo $slideimage; ?>);">

						<div class="text_block">
								<?php $surl = get_sub_field("url"); ?>
								<?php if($surl){ ?>

								<a href="<?php echo $surl; ?>">
									<h2>
								<?php the_sub_field("title"); ?>
								</h2>
								</a>
								<?php } else { ?>
								<h2><?php the_sub_field("title"); ?></h2>
								<?php } ?>
									
								

							<a href="<?php the_permalink(get_option( 'woocommerce_shop_page_id' )); ?>" class="banner_btn"><?php _e( 'View our Products', 'twentyseventeen' ); ?></a>

						</div>

					</div><?php

					$j++;

				}	?>

			</div>

		</div><?php

	}	?>

</section>