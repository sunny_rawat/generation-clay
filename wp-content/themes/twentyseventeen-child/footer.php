<?php

/**

 * The template for displaying the footer

 *

 * Contains the closing of the #content div and all content after.

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package WordPress

 * @subpackage Twenty_Seventeen

 * @since 1.0

 * @version 1.2

 */



?>
 
<div class="instagramfeads">
	<?php echo instagram_feeds_custom(); ?>
	</div>
	<div class="mobile-instagram-feed">
		<?php echo '<a target="_blank" href="'.get_field('instgram_url','option').'"><p>'.get_field('instgram_username','option').'</p><p>'.get_field('instgram_follow_us','option').'</p></a>'; ?>
	</div>

 

<footer>

	<div class="container">

		<div class="row">

			<div class="col-md-3 col-sm-3">

				<a href="<?php echo site_url(); ?>" class="foot_logo"><img src="<?php the_field('footer_logo','option'); ?>"></a>

			 
<div class="hide_on_mob">
		 <?php if( have_rows('social_icons','option') ) {	?>

				<div class="media_icons">

					<?php while ( have_rows('social_icons','option') ) {  the_row(); ?>

						<a href="<?php the_sub_field("url_social"); ?>" target="_blank"><i class="fa <?php the_sub_field("icon_social"); ?>" aria-hidden="true"></i></a>

				<?php } ?>

				</div>

	<?php } ?>

 



	<?php the_field('footer_text','option'); ?>
</div>
<div class="show_on_mob">
				<?php dynamic_sidebar('sidebar-2'); ?>
</div>
			</div>

			<div class="col-md-2 col-sm-2">

				<h3><?php _e( 'Links', 'twentyseventeen' ); ?></h3>

				<?php wp_nav_menu( array(

					'menu_class'     => 'footer_menu',

					'theme_location' => 'footer-menu',

					'container'      => 'ul',

				) ); ?>

				 

			</div>

			<div class="col-md-2 col-sm-2">

				<h3><?php _e( 'Information', 'twentyseventeen' ); ?></h3>

				<?php wp_nav_menu( array(

					'menu_class'     => 'information_menu',

					'theme_location' => 'footer-information',

					'container'      => 'ul',

				) ); ?>

			</div>

			<div class="col-md-3 col-sm-3">
				<div class="hide_on_mob">
				<?php dynamic_sidebar('sidebar-2'); ?>
</div>
				<div class="show_on_mob">
		 <?php if( have_rows('social_icons','option') ) {	?>

				<div class="media_icons">

					<?php while ( have_rows('social_icons','option') ) {  the_row(); ?>

						<a href="<?php the_sub_field("url_social"); ?>" target="_blank"><i class="fa <?php the_sub_field("icon_social"); ?>" aria-hidden="true"></i></a>

				<?php } ?>

				</div>

	<?php } ?>

 



	<?php the_field('footer_text','option'); ?>
</div>
			</div>

			<div class="col-md-2 col-sm-2">

				<a href="#top" class="scrolltotop"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

			</div>

		</div>

	</div>

</footer>
<?php //dynamic_sidebar('sidebar-3'); ?>
<?php wp_footer(); ?>
 
</body>

</html>

