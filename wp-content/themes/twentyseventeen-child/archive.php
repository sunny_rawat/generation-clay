<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header('blog'); ?>
<section class="blog-sec detail-item-2 archive">
	<div class="container">
		<div class="top-heading">
			<h2><?php //echo the_archive_title(); ?></h2>
		</div>
		<?php if ( have_posts() ) : ?>
		<ul>
		    <?php global $post; $loop = 0; ?>
				<?php 
				while ( have_posts() ) : the_post();
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'custom-size' );  
				$cat = get_the_category($post->ID);
				$cat_name =  $cat[0]->name;
				?>
    			<li class="col-md-4">
    				<div class="blog-item">
    					<h5><a href="<?php echo get_category_link( $cat[0]->term_id ); ?>"><?php echo $cat_name; ?></a></h5>
    					<figure><a href="<?php the_permalink(); ?>"><img src="<?php echo $image[0]; ?>"/></a></figure>
    					<h3><a href="<?php the_permalink(); ?>"><?php echo substr(get_the_title(),0, 35); ?></a></h3>
    					<div class="date-item">
                          <p><?php echo get_the_date( 'M d, Y', $post->ID );?></p>
                          |
                          <p><?php the_field('reading_time', $post->ID);?></p>
                        </div>
    				</div>
    			</li>
    			<?php if(($loop%3) == 2 ){?>
    			    </ul><hr /><ul>
    			<?php } ?>
            <?php $loop++; endwhile; ?>
		</ul>
		<?php wp_pagenavi(); ?>
		<?php
			else :

			get_template_part( 'template-parts/post/content', 'none' );

		endif; ?>
	</div>
</section>
<section class="detail_bottom">
	<div class="container">
		<div class="inner-item">
			<div class="inner-left">
				<h2><?php the_field('heading', 'option');?></h2>
				<a href="<?php the_field('download_our_voice_magazine_url', 'option');?>"><?php the_field('download_our_voice_magazine_text', 'option');?></a>
			</div>
			<div class="inner-right"><figure><img src="<?php the_field('banner', 'option');?>" /></figure>
			</div>
		</div>
	</div>
</section>
<?php get_footer();
