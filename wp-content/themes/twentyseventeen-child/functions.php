<?php

define( 'CHILD_DIR', get_stylesheet_directory_uri() );
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'    => 'Home banner',
        'menu_title'    => 'Home banner',
        'menu_slug'     => 'home-banner-slides',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));

  acf_add_options_page(array(
        'page_title'    => 'Theme Settings',
        'menu_title'    => 'Theme Settings',
        'menu_slug'     => 'theme-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
}

add_image_size( 'custom-size', 379, 262 );

add_filter( 'get_the_archive_title', function ($title) {
    if ( is_category() ) {
            $title = single_cat_title( '', false );
        } elseif ( is_tag() ) {
            $title = single_tag_title( '', false );
        } elseif ( is_author() ) {
            $title = '<span class="vcard">' . get_the_author() . '</span>' ;
        }
    return $title;
});

show_admin_bar( false );

add_filter('upload_mimes', 'svg_myme_types', 1, 1);
function svg_myme_types($mime_types){ 
    $mime_types['svg'] = 'image/svg+xml'; //Adding svg extension
    return $mime_types;
}

add_action('login_head', 'theme_favicon');
add_action('admin_head', 'theme_favicon');
add_action('wp_head', 'theme_favicon');
function theme_favicon() {
    if(get_field('favicon_logo', 'option')){
        echo '<link rel="shortcut icon" type="image/png" href="'. get_field('favicon_logo', 'option').'" />';
    }
}

register_nav_menus( array(
    'footer-menu' => __( 'Footer Menu', 'twentyseventeen' ),
    'footer-information' => __( 'Footer Information', 'twentyseventeen' ),
) );

add_filter('excerpt_more', 'remove_excerpt_more', 21 );
function remove_excerpt_more($more) {
    return '';
}

//add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function custom_excerpt_length( $length ) {
    return 25;
}

add_filter('posts_orderby', 'alpha_order_stockist', 10, 2);
function alpha_order_stockist($orderby, $query){
    global $wpdb;
    if (get_query_var('post_type') == 'stockist') {
         return "$wpdb->posts.post_title ASC";
    }
    return $orderby;
 }

add_action( 'pre_get_posts', function ( $query ) {
  if ( $query->is_post_type_archive( 'stockist' ) && ! is_admin() ) {
    $query->set( 'posts_per_page', -1 );
  }
} );


add_filter('loop_shop_columns', 'loop_columns', 999);
function loop_columns() {
    return 2;
}

// Move WooCommerce price
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 25 );


add_filter( 'wp_nav_menu_items', 'top_menu_link', 10, 2 );
function top_menu_link( $item, $args ) {
    if ($args->theme_location == 'top') {
        $shop = woocommerce_get_page_id('shop');
        $items .= '<li class="has_child"><a href="'.get_permalink($shop).'">'.get_the_title($shop).'</a> <span class="product-mobile-arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></span>';
           $args = array(
                    'post_type' => 'product',
                    'posts_per_page' => -1,
                    'post_status' => array('publish'),      
                );
        $loop = new WP_Query( $args );
        if ( $loop->have_posts() ) {
            if ( wp_is_mobile() ) {
                $items .=  '<ul class="submenu" style="display:none;">';
                while ( $loop->have_posts() ) { $loop->the_post();
                    global $product;
                    $product = get_product( get_the_ID() );
                    $items .= '<li class="item"><a href="'.get_post_permalink().'">'.get_the_title().'</a></li>';
                }  wp_reset_query();
            $items .=  '</ul>';
            } else {
               $items .=  '<ul class="submenu owl-carousel owl-theme product-carousel">';
                    while ( $loop->have_posts() ) { 
                        $loop->the_post();
                        global $product;
                        $product = get_product( get_the_ID() );
                        if ( has_post_thumbnail( get_the_ID() ) ) {
                            $productImg = get_the_post_thumbnail( get_the_ID(), 'shop_single' );
                        } else {
                            $productImg = '<img src="' . woocommerce_placeholder_img_src() . '" alt="Placeholder" />';
                        } 
                        $items .= '<li class="item"><a href="'.get_post_permalink().'"><figure>'.$productImg.'</figure>'.get_the_title().'</a></li>';
                    }  wp_reset_query();
                $items .=  '</ul>'; 
            }  
        }  
        $items .= '</li>';
        $item = $items.$item;
   }
    return $item;
}

add_action( 'wp_ajax_nopriv_mini_cart_html_ajax', 'mini_cart_html_ajax' );
add_action( 'wp_ajax_mini_cart_html_ajax', 'mini_cart_html_ajax' );
function mini_cart_html_ajax() {
   woocommerce_get_template( 'cart/mini-cart.php');
   die();
}


add_action( 'wp_ajax_nopriv_mini_cart_total', 'mini_cart_total' );
add_action( 'wp_ajax_mini_cart_total', 'mini_cart_total' );
function mini_cart_total() {
    echo WC()->cart->get_cart_contents_count();
    die();
}

add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_before_image_shop', 5 );
function woocommerce_before_image_shop() {
     $hoverImage = get_field('product_hover_image', get_the_ID());
     if($hoverImage){
        $shophover = 'shop-hover-active';
     } else {
        $shophover = '';
     }
   echo '<div class="shop-image-bundel '.$shophover.'">'; 
    $product_badge = get_field('product_badge', get_the_ID());
        if($hoverImage){
            echo '<img src="'.$hoverImage['sizes']['shop_catalog'].'" class="attachment-woocommerce_hover-image size-woocommerce_thumbnail wp-post-image" alt="" width="250" height="250" data-hover="yes">';
        }
        if($product_badge){
            echo '<img src="'.$product_badge.'" class="attachment-woocommerce_product_badge size-woocommerce_thumbnail wp-post-image" alt="">';
        }
 }


add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_after_image_shop', 5 );
function woocommerce_after_image_shop() {
    echo '</div>';
    echo '<div class="mobile-title"><h2 class="woocommerce-mobile-product__title">'. get_the_title().'</h2></div>';
 
}

add_action('woocommerce_before_shop_loop_item','woocommerce_background_image_shop', 20);
function woocommerce_background_image_shop(){
    $BackgroundImage = get_field('bacground_image', get_the_ID());
    echo '<div class="loopbackgroundimg">';
    if($BackgroundImage){
    	echo '<img src="' . $BackgroundImage . '" />';
    } else {
    	echo '<img src="' . woocommerce_placeholder_img_src() . '" alt="Placeholder" />';
    }
    echo '</div>';
}

add_action( 'woocommerce_after_single_product_summary', 'woocommerce_before_description');
function woocommerce_before_description() { ?>
    <div class="woocommerce-description">
         <?php if( have_rows('production_information') ) {    ?>
        <div class="woocommerce-product-info">
            <h3><?php _e( 'Product Information', 'twentyseventeen' ); ?></h3>
                    <ul>
                        <?php while ( have_rows('production_information') ) {  the_row(); ?>
                          <li> <img src="<?php the_sub_field("production_icon"); ?>">
                            <h4><?php the_sub_field("production_heading"); ?></h4>
                            <p><?php the_sub_field("production_detail"); ?></p> </li>
                    <?php } ?>
                    </ul>
        </div>
         <?php } ?>
           <?php if( have_rows('key_benefits') ) {    ?>
        <div class="woocommerce-key-benefits">
            <h3><?php _e( 'Key benefits', 'twentyseventeen' ); ?></h3>
                    <ul>
                        <?php while ( have_rows('key_benefits') ) {  the_row(); ?>
                          <li> <img src="<?php the_sub_field("key_icon"); ?>">
                            <h4><?php the_sub_field("key_heading"); ?></h4>
                            <p><?php the_sub_field("key_detail"); ?></p></li>
                    <?php } ?>
                    </ul>
        </div>
        <?php } ?>
        <?php if(get_field('ingredients_text')){ ?>
        <div class="woocommerce-ingredients">
            <h3><?php _e( 'Ingredients', 'twentyseventeen' ); ?></h3>
            <p><?php the_field('ingredients_text'); ?></p>
        </div>
        <?php } ?>
    </div>
    <?php if(get_field('youtbe_url')){ ?>
    <div class="how-to-apply-video">
        <h3><?php _e( 'How to Apply', 'twentyseventeen' ); ?></h3>
        <div class="video-section">
            <iframe  class="video-frame" src="<?php the_field('youtbe_url'); ?>"> </iframe> 
        </div>
    </div>
     <?php } ?>
     <div class="woo-product-reviews-widget">
     	<h2><?php _e( 'What people are saying', 'twentyseventeen' ); ?></h2>
     	<?php wc_yotpo_show_widget(); ?>
     </div>
<?php 
 }

add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
  function jk_related_products_args( $args ) {
    $args['posts_per_page'] = 3; 
    $args['columns'] = 3; 
    return $args;
}


add_filter( 'woocommerce_checkout_fields' , 'custom_rename_wc_checkout_fields' );
function custom_rename_wc_checkout_fields( $fields ) {
    $fields['billing']['billing_first_name']['placeholder'] = 'First Name';
    $fields['billing']['billing_last_name']['placeholder'] = 'Last Name';
    $fields['billing']['billing_company']['placeholder'] = 'Company';
    $fields['billing']['billing_city']['placeholder'] = 'City';
    $fields['billing']['billing_state']['placeholder'] = 'State';
    $fields['billing']['billing_postcode']['placeholder'] = 'Postal Code';
    $fields['billing']['billing_phone']['placeholder'] = 'Phone';
    $fields['billing']['billing_email']['placeholder'] = 'Email';
    $fields['shipping']['shipping_first_name']['placeholder'] = 'First Name';
    $fields['shipping']['shipping_last_name']['placeholder'] = 'Last Name';
    $fields['shipping']['shipping_company']['placeholder'] = 'Company';
    $fields['shipping']['shipping_city']['placeholder'] = 'City';
    $fields['shipping']['shipping_state']['placeholder'] = 'State';
    $fields['shipping']['shipping_postcode']['placeholder'] = 'Postal Code';
    $fields['shipping']['shipping_phone']['placeholder'] = 'Phone';
    $fields['shipping']['shipping_email']['placeholder'] = 'Email';
return $fields;
} 

remove_action('woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20);
add_action( 'woocommerce_checkout_after_order_review', 'woocommerce_checkout_payment', 20 );

add_action( 'wp_enqueue_scripts', 'theme_jquery_load_scripts' );
function theme_jquery_load_scripts() {
    global $wp_query; 
    wp_enqueue_style( 'bootstrap-css', CHILD_DIR . '/css/bootstrap.min.css' );
    wp_enqueue_style( 'custom-style', CHILD_DIR . '/css/style.css' );
    wp_enqueue_style( 'owl-carousel-css', CHILD_DIR . '/css/owl.carousel.min.css' );
    wp_enqueue_script( 'bootstrap-script', CHILD_DIR . '/js/bootstrap.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'bodycommerce-add-to-cart-ajax', CHILD_DIR . '/js/add-to-cart-ajax.js', array('jquery'), '', true );
    wp_enqueue_script( 'isotope-jquery', CHILD_DIR . '/portfolio/isotope.js', array('jquery'), '', true );
    wp_enqueue_script( 'owl-carousel-script', CHILD_DIR . '/js/owl.carousel.min.js' , '', '', true );
    wp_enqueue_script( 'custom-script', CHILD_DIR . '/js/custom.js', array('jquery'), '', true );
    wp_enqueue_script( 'pbd-alp-style', CHILD_DIR . '/js/load-posts.js' , array('jquery'), '', true );
    $arrayloading  =  array(
            'ajaxurl' => admin_url('admin-ajax.php'), // WordPress AJAX
            'ajaxloadingimg' => '', // WordPress AJAX
            'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
            'max_page' => $wp_query->max_num_pages
        );
    echo '<script> var loadingelements = '.json_encode($arrayloading).'; </script>';   
}

add_action('wp_ajax_blog_loadmore', 'blog_loadmore_ajax_handler');
add_action('wp_ajax_nopriv_blog_loadmore', 'blog_loadmore_ajax_handler');
function blog_loadmore_ajax_handler(){
    $paged = $_POST['page'] + 1;
    $newsArgs = array(
        'post_type' => 'blog', 
        'post_status' => array('publish'), 
        'posts_per_page' => get_option( 'posts_per_page' ),
        'paged' => $paged
    );
    $newsQuery = new WP_Query( $newsArgs);
   if ( $newsQuery->have_posts() ) 
    {
        while ( $newsQuery->have_posts() )   {   $newsQuery->the_post(); ?>
            <div class="col-md-6">
                <div class="block home_blog">
                    <?php 
                        if ( has_post_thumbnail() ) : ?>
                               <figure> <?php the_post_thumbnail(); ?><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"></a></figure>
                            <?php 
                        endif; ?>
                    <div class="title"> <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                    <div class="date"><?php _e( 'Blog – ', 'twentyseventeen' ); ?><?php echo get_the_date( 'd/m/Y', $post->ID );?></div>
                </div>
            </div>
        <?php 
        }
   } 
    die();
}

add_action('wp_ajax_community_loadmore', 'community_loadmore_ajax_handler');
add_action('wp_ajax_nopriv_community_loadmore', 'community_loadmore_ajax_handler');
function community_loadmore_ajax_handler(){
    $paged = $_POST['page'] + 1;
    $newsArgs = array(
        'post_type' => 'communities', 
        'post_status' => array('publish'), 
        'posts_per_page' => get_option( 'posts_per_page' ),
        'paged' => $paged
    );
    $newsQuery = new WP_Query( $newsArgs);
   if ( $newsQuery->have_posts() ) :
  
        while ( $newsQuery->have_posts() ) : $newsQuery->the_post();
            $image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'custom-size' );  
                $cat = wp_get_post_terms(get_the_ID(),'community-categories');
                $cat_id =  $cat[0]->term_id;
                if($cat_id!=34){
                    $newclass = 'home_blog';
                    $newtitle = get_the_title();
                    $permalink  = get_post_permalink();
                    $linktarget  = '';
                } else {
                    $newclass = '';
                    $newtitle = get_field('community_external_url');
                    $newtitle = preg_replace('#^http?://#', '', $newtitle);
                    $newtitle = preg_replace('#^https?://#', '', $newtitle);
                    $newtitle = preg_replace('#^www.#', '', $newtitle);
                    $permalink  = get_field('community_external_url');
                    $linktarget  = 'target="_blank"';
                }

                $catsalug = array();
                foreach($cat as $catdata){
                    $catsalug[] = $catdata->slug;
                }
                $implodeslug = implode(' ', $catsalug); ?>
                <article class="portfolio-item <?php echo $implodeslug; ?> col-xs-12 col-sm-6 col-md-6" style="">
                 <div class="block <?php echo $newclass; ?>">
                    <figure>
                        <img src="<?php echo $image[0]; ?>"><a href="<?php echo $permalink; ?>" <?php echo $linktarget; ?>></a>
                    </figure>
                    <div class="title"><a href="<?php echo $permalink; ?>" <?php echo $linktarget; ?>><?php echo $newtitle; ?></a></div>
                    <div class="date"><?php echo $cat[0]->name; ?> – <?php the_date('m/d/Y'); ?></div>
                </div>
              </article>
     <?php endwhile;  
    endif; 
    die();
}

add_action('wp_ajax_results_loadmore', 'results_loadmore_ajax_handler');
add_action('wp_ajax_nopriv_results_loadmore', 'results_loadmore_ajax_handler');
function results_loadmore_ajax_handler(){
    $paged = $_POST['page'] + 1;
    $newsArgs = array(
        'post_type' => 'results', 
        'post_status' => array('publish'), 
        'posts_per_page' => get_option( 'posts_per_page' ),
        'paged' => $paged
    );
    $newsQuery = new WP_Query( $newsArgs);
   if ( $newsQuery->have_posts() ) 
    {
        while ( $newsQuery->have_posts() )   {   $newsQuery->the_post(); 
            $external_url = get_field('external_url'); ?>
            <div class="col-md-3 blockup">
            <div class="block">
                <figure><?php
                    if ( has_post_thumbnail() ) : 
                        the_post_thumbnail();  
                    endif; ?>
                </figure>
                <div class="text_block">
                    <h4><a href="<?php echo $external_url; ?>" target="_blank"><?php the_field('result_username'); ?></a></h4>
                    <?php the_excerpt(); ?>
                </div>
            </div>
        </div>
        <?php 
        }
   }
    die();
}

/* Instagram */
function rudr_instagram_api_curl_connect( $api_url ){
    $connection_c = curl_init(); // initializing
    curl_setopt( $connection_c, CURLOPT_URL, $api_url ); // API URL to connect
    curl_setopt( $connection_c, CURLOPT_RETURNTRANSFER, 1 ); // return the result, do not print
    curl_setopt( $connection_c, CURLOPT_TIMEOUT, 20 );
    $json_return = curl_exec( $connection_c ); // connect and get json data
    curl_close( $connection_c ); // close connection
    return json_decode( $json_return ); // decode and return
}

function instagram_feeds_custom() {
    $outputs = '';
    $instagram_access_token = get_field('instagram_access_token', 'option');
    $instagram_counts = get_field('instagram_counts', 'option');
    $return = rudr_instagram_api_curl_connect("https://api.instagram.com/v1/users/self/media/recent?access_token=".$instagram_access_token."&count=".$instagram_counts);
    $outputs .= '<div class="intgramheading"><div class="intgram-heading-inner"><h2>'.get_field('instagram_heading','option').'</h2> <a target="_blank" href="'.get_field('instgram_url','option').'"><p>'.get_field('instgram_username','option').'</p><p>'.get_field('instgram_follow_us','option').'</p></a></div></div>';
    $outputs .='<div class="owl-carousel owl-theme" id="owl-carousel">';
    foreach ($return->data as $instagrampost) { 
        $outputs .= '<div class="item"> <a target="_blank" href="'.$instagrampost->link.'"><img src="'. $instagrampost->images->low_resolution->url.'" /></a></div>';
     }
    $outputs .='</div>';
return $outputs;     
}

add_action( 'wp_ajax_product_remove', 'warp_ajax_product_remove' );
add_action( 'wp_ajax_nopriv_product_remove', 'warp_ajax_product_remove' );
function warp_ajax_product_remove()
{
    ob_start();
    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item)
    {
        if($cart_item['product_id'] == $_POST['product_id'] && $cart_item_key == $_POST['cart_item_key'] )
        {
            WC()->cart->remove_cart_item($cart_item_key);
        }
    }

    WC()->cart->calculate_totals();
    WC()->cart->maybe_set_cart_cookies();
    woocommerce_mini_cart();
    $mini_cart = ob_get_clean();
    $data = array(
        'fragments' => apply_filters( 'woocommerce_add_to_cart_fragments', array(
                'div.widget_shopping_cart_content' => '<div class="widget_shopping_cart_content">' . $mini_cart . '</div>'
            )
        ),
        'cart_hash' => apply_filters( 'woocommerce_add_to_cart_hash', WC()->cart->get_cart_for_session() ? md5( json_encode( WC()->cart->get_cart_for_session() ) ) : '', WC()->cart->get_cart_for_session() )
    );
    wp_send_json( $data );
    die();
}

add_action('wp_ajax_mini_cart_qty_cart', 'mini_cart_qty_cart');
add_action('wp_ajax_nopriv_mini_cart_qty_cart', 'mini_cart_qty_cart');
function mini_cart_qty_cart() { 
    $cart_item_key = $_POST['hash'];
    $threeball_product_values = WC()->cart->get_cart_item( $cart_item_key );
    $threeball_product_quantity = apply_filters( 'woocommerce_stock_amount_cart_item', apply_filters( 'woocommerce_stock_amount', preg_replace( "/[^0-9\.]/", '', filter_var($_POST['quantity'], FILTER_SANITIZE_NUMBER_INT)) ), $cart_item_key );
    $passed_validation  = apply_filters( 'woocommerce_update_cart_validation', true, $cart_item_key, $threeball_product_values, $threeball_product_quantity );
    if ( $passed_validation ) {
        WC()->cart->set_quantity( $cart_item_key, $threeball_product_quantity, true );
    }
    die();
}

add_action( 'woocommerce_single_product_summary', 'woo_single_product_title_text', 1);
function woo_single_product_title_text(){
    echo '<div class="mobile-addto-cart"><a href="javascript:void(0)" class="button">Add to Cart</a></div>';
    echo '<div class="before-title-text">Products</div>';
}

add_filter('woocommerce_default_catalog_orderby', 'custom_default_catalog_orderby');
function custom_default_catalog_orderby() {
     return 'date';
}

add_filter( 'woocommerce_email_styles', 'editing_woocommerce_email_styles' );
function editing_woocommerce_email_styles( $css ) {
    $css .= "#template_header { background-color: #fff!important; }";
    $css .= "#template_header h1 { color: #3c3c3c!important; }";
    $css .= "#header_wrapper { padding: 40px 48px 0;}";
    $css .= "#template_header_image p { margin-bottom: 0px!important; }";
    $css .= "#template_container { border: none; border-radius: 0!important; margin-top: -6px!important; border-color: transparent; }";
    $css .= "#body_content { padding-bottom: 10px; }";
    $css .= "#template_container h2 { color: #c7c9d8!important; font-weight: 400!important; text-transform: uppercase; letter-spacing: 1.5px; font-size: 12px!important; margin: 30px 0 10px!important; }";
    $css .= "#template_footer { background: white; color: black; }";
    $css .= "#template_footer p { color: #797979; text-transform: uppercase; font-weight: 100; letter-spacing: 1.2px; }";
return $css;
}