<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header('blog'); ?>

<?php if(is_shop()){ 
		$shop = woocommerce_get_page_id('shop');
		if( has_post_thumbnail( $shop ) ) { 
				$shop_page = get_post($shop); 
				$shoppage_content = $shop_page->post_content;
				$shoppage_content = apply_filters('the_content', $shoppage_content);
				$shoppage_content = str_replace(']]>', ']]>', $shoppage_content);

				$cover_mobile = get_field('mobile_banner_image',$shop);
			?>
		<section class="shop_banner">
			<img src="<?php echo get_the_post_thumbnail_url($shop); ?>" class="cover_img">
			<img src="<?php echo $cover_mobile; ?>" class="cover_mobile">
			<div class="text_block">
				<h1><?php woocommerce_page_title(); ?></h1>
				<?php echo $shoppage_content; ?>
			</div>
		</section>
<?php } } ?>


<section class="result_woocomerce">
	<main class="container" role="main">
		<?php woocommerce_content(); ?>
	</main>
</div>
<?php get_footer();
