<?php get_header(); ?>

<section class="blurb_sec">
	<div class="container">

		<?php  if( have_rows('section_2_contents') ): 
		while ( have_rows('section_2_contents') ) : the_row(); ?>
		 <div class="col-md-3">
			<div class="block">
				<figure>
					<img src="<?php echo get_sub_field('section_2_image'); ?>">
				</figure>
				<div class="text_block">
					<h3><?php echo get_sub_field('section_2_title'); ?></h3>
					<p><?php echo get_sub_field('section_2_content'); ?></p>
				</div>
			</div>
		</div>
		 <?php endwhile;
		  endif; ?> 
	</div>
</section>


<section class="home_sec1">
	<div class="container">
		<div class="text_in">
		<h2><?php echo get_field('section_3_title'); ?></h2>
			<?php echo get_field('section_3_content'); ?>
	</div>
	</div>
</section><?php

$args = array(
    'post_type' => 'product',
    'posts_per_page' => -1,
    'meta_query' => array(
        array(
            'key' => 'featured',
            'value' => 'yes',
			'compare' => 'LIKE'		
        )
    )
    );
$loop = new WP_Query( $args );
if ( $loop->have_posts() ) 
{	?>
	<section class="home_productsec"><?php
	$pr = 1;
		while ( $loop->have_posts() ) 
		{ 
			$loop->the_post();
			global $product;
        	$product = get_product( get_the_ID() ); 
        	$hoverImage = get_field('product_hover_image', get_the_ID());
        	 ?>
			<div class="product_home <?php echo 'featured-product-home-'.$pr; ?>">
				<div class="container-fluid">
					<div class="row">
					<div class="cover_pic">
						<?php $getimag = get_field('bacground_image');
						if(!empty($getimag))
						{ 	
							echo '<img src="'.$getimag.'">';
						}
						else
						{ 
							echo '<img src="' . woocommerce_placeholder_img_src() . '" alt="Placeholder" />';
						} ?>
					</div>
					<div class="product__sec" data-scroll-speed="10">
						<h3 class="hide_on_mob"><?php the_title(); ?></h3>
						<figure class="product_home_images <?php if($hoverImage){ echo 'hoveractive'; } ?>">
							<a href="<?php the_permalink(); ?>">
							<?php 
							

							if($hoverImage){
         					   echo '<img src="'.$hoverImage['sizes']['shop_single'].'" class="attachment-woocommerce_hover-home attachment-shop_single size-shop_single wp-post-image" alt="" width="318" height="298" data-hover="yes">';

       						}

							$product_badge = get_field('product_badge', get_the_ID());
							if($product_badge){
					            echo '<img src="'.$product_badge.'" class="attachment-woocommerce_product_badge size-woocommerce_thumbnail wp-post-image" alt="">';
					        }
							if ( has_post_thumbnail( get_the_ID() ) ) {
								echo get_the_post_thumbnail( get_the_ID(), 'shop_single', array('class' => 'home-pro-image') );
							} else {
								echo '<img src="' . woocommerce_placeholder_img_src() . '" alt="Placeholder" />';
							} 	?>
						</a>
						</figure>
						<h3 class="show_on_mob"><?php the_title(); ?></h3>
						<p class="price"><?php 
							echo $product->get_price_html(); ?> – <?php 
							if ( $product->is_in_stock() ) $availability['availability'] = __('In Stock', 'woocommerce');
  							if ( !$product->is_in_stock() ) $availability['availability'] = __('Out Of Stock', 'woocommerce');
	  						echo $availability['availability'];
   ?></p>
						<?php woocommerce_template_loop_add_to_cart(); ?>
					</div>
				</div></div>
			</div><?php
		$pr++; } wp_reset_query(); ?>
</section><?php
} ?>
<section class="partner_sec">
	<div class="container">
		<h2><?php echo get_field('our_partner_title'); ?></h2>
		<ul>
 		<?php  if( have_rows('our_partners') ): 
		while ( have_rows('our_partners') ) : the_row(); ?>
		<li><a href="<?php echo get_sub_field('partner_logo'); ?>"><img src="<?php echo get_sub_field('partner_logo'); ?>"></a></li>
		 <?php endwhile;
		  endif; ?> 
			
		</ul>
	</div>
</section>

<section class="foot_banner">
	<a href="<?php echo site_url('communities'); ?>">
	<img src="<?php echo get_field('community_banner'); ?>">
	<h2><?php echo get_field('community_title'); ?></h2>
	</a>
</section>

<section class="home_latest">
	<div class="container">
		<div class="row">
<?php $resultsargs = array(
	    'post_type' => 'results',
	    'posts_per_page' => 1,
	    'post_status' => 'publish',
    );
$resultsloop = new WP_Query( $resultsargs );
if ( $resultsloop->have_posts() ) :	
	while ( $resultsloop->have_posts() ) :  $resultsloop->the_post();
	?>

			<div class="col-md-6">
				<h2><?php _e( 'REAL LIFE RESULTS', 'twentyseventeen' ); ?></h2>
				<div class="block">

					<?php if ( has_post_thumbnail( get_the_ID() ) ) { ?>
					<figure>
						<img src="<?php echo get_the_post_thumbnail_url(); ?>">
						<a href="<?php echo site_url('result'); ?>"></a>
					</figure>
					<?php } ?>

					<div class="title"><a href="<?php echo site_url('result'); ?>"><?php the_title(); ?></a></div>
					<div class="date"><?php _e( 'Results –', 'twentyseventeen' ); ?></div>
				</div>
			</div>
<?php  endwhile; wp_reset_query(); endif; ?>

<?php $blogargs = array(
	    'post_type' => 'blog',
	    'posts_per_page' => 1,
	    'post_status' => 'publish',
    );
$blogloop = new WP_Query( $blogargs );
if ( $blogloop->have_posts() ) :	
	while ( $blogloop->have_posts() ) :  $blogloop->the_post();
	?>
			<div class="col-md-6">
				<h2><?php _e( 'WORLD OF GEN CLAY<sup>TM</sup>', 'twentyseventeen' ); ?></h2>
				<div class="block home_blog">
					<?php if ( has_post_thumbnail( get_the_ID() ) ) { ?>
					<figure>
						<img src="<?php echo get_the_post_thumbnail_url(); ?>">
						<a href="<?php the_permalink(); ?>"></a>
					</figure>
					<?php } ?>
					<div class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
					<div class="date"><?php _e( 'Blog – ', 'twentyseventeen'); ?><?php the_date('m/d/Y'); ?></div>
				</div>
			</div>
 	<?php  endwhile; 
 	wp_reset_query();
 	endif; ?>

		</div>
	</div>
</section>
<?php get_footer();