<?php

/**

 * The template for displaying archive pages

 *

 * @link https://codex.wordpress.org/Template_Hierarchy

 *

 * @package WordPress

 * @subpackage Twenty_Seventeen

 * @since 1.0

 * @version 1.0

 */



get_header('blog'); ?>


<?php $getpage = get_page(99); 
?>
 <section class="stockists_page">
	<div class="container">
		<div class="cover_img"><img src="<?php echo get_field('banner_image',$getpage); ?>"></div>
		<h1><?php echo get_the_title($getpage->ID); ?></h1>

<?php if ( have_posts() ) : 
		while ( have_posts() ) : the_post(); ?>

		<h2><?php the_title(); ?></h2>
		<?php if( have_rows('stockists_list') ) {	?>
		<div class="row">
			<?php while ( have_rows('stockists_list') ) {  the_row(); ?>
		<div class="col-md-2">
			<div class="block">
			<h4><?php the_sub_field("title"); ?></h4>
			<p><?php the_sub_field("address"); ?></p>
		</div>
		</div>
		<?php } ?>
	</div>
	<?php }
	endwhile; 
	else :
		get_template_part( 'template-parts/post/content', 'none' );
	endif;  ?>

	</div>
</section>


<section class="become_strockist">
	<div class="cover_img" style="background-image: url(<?php echo get_the_post_thumbnail_url($getpage->ID); ?>);"></div>
	<div class="block">
		<?php echo apply_filters('the_content', $getpage->post_content); ?>
	</div>
</section>

<?php get_footer();