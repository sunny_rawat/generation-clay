<?php
get_header('blog'); ?>
<script type="text/javascript">
(function() {
    window.PinIt = window.PinIt || { loaded:false };
    if (window.PinIt.loaded) return;
    window.PinIt.loaded = true;
    function async_load(){
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.async = true;
        s.src = "http://assets.pinterest.com/js/pinit.js";
        var x = document.getElementsByTagName("script")[0];
        x.parentNode.insertBefore(s, x);
    }
    if (window.attachEvent)
        window.attachEvent("onload", async_load);
    else
        window.addEventListener("load", async_load, false);
})();
</script>
<section class="blog_post">
	<div class="container">
		<div class="date1"><?php _e( 'Blog  |  ', 'twentyseventeen' ); ?><?php echo get_the_date( 'm/d/Y');?></div>
		<div class="banner_img">
			<img src="<?php echo get_the_post_thumbnail_url(); ?>">
		</div>
		<div class="post_in">
			<?php while ( have_posts() ) : the_post(); ?>
			<h1><?php the_title(); ?></h1>
			<?php the_content(); ?>
 ?>

<div class="share_post">
<?php $title = get_the_title();
$url= get_the_permalink();
$summary=urlencode(get_the_content());

$attachment_idpost = get_post_thumbnail_id(get_the_ID());
$imagepost = wp_get_attachment_image_src( $attachment_idpost, 'thumbnail' );
 ?>
 

	<?php _e( 'Share:', 'twentyseventeen' ); ?> 

<a id="ref_pr" href="http://pinterest.com/pin/create/bookmarklet/?media=<?php echo $imagepost[0];?>&amp;
url=<?php echo $url; ?>&amp;
is_video=false&amp;description=<?php echo $title;?>"
onclick="javascript:window.open(this.href, '_blank', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');return false;"><i class="fa fa-pinterest" aria-hidden="true"></i></a>

 <a class="fb-share" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>&picture=<?php echo $imagepost[0];?>" target="_blank">
    <i class="fa fa-facebook" aria-hidden="true"></i>
</a>

<a class="tw-share" target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo $url; ?>&amp;text=<?php echo $title;?>&amp;via=<?php bloginfo('name'); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
</div>
<?php endwhile; ?>
	
	<?php  $next_post = get_next_post(); 
		if($next_post){ ?>
				<section class="home_latest">
					<div class="row">
						<div class="col-md-12">
							<h2><?php _e( 'Next post', 'twentyseventeen' ); ?></h2>
			         				<div class="block home_blog">
			                        	<figure>
			                        		<img src="<?php echo get_the_post_thumbnail_url($next_post->ID); ?>" />
			                        		<a href="<?php echo $next_post->guid; ?>"></a>
			                        	</figure>
			                    		<div class="title"><a href="<?php echo get_post_permalink($next_post->ID); ?>"><?php echo $next_post->post_title; ?></a></div>
			                    		<div class="date"><?php _e( 'Blog – ', 'twentyseventeen' ); ?> <?php echo get_the_date( 'm/d/Y', $next_post->ID); ?></div>
			                		</div>
						</div>
					</div>
				</section>
		<?php 
     	} ?>
		</div>
	</div>
</section>
<?php get_footer();