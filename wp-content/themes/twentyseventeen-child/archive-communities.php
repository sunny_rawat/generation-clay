<?php

/**

 * The template for displaying archive pages

 *

 * @link https://codex.wordpress.org/Template_Hierarchy

 *

 * @package WordPress

 * @subpackage Twenty_Seventeen

 * @since 1.0

 * @version 1.0

 */



get_header('blog'); ?>
<?php $getpage = get_page(180);  
$page_content = $getpage->post_content;
				$page_content = apply_filters('the_content', $page_content);
				$page_content = str_replace(']]>', ']]>', $page_content);
?>
<style type="text/css">
	/* #portfolio .cajaxappending{margin-right: 0; position: relative;}
	#portfolio .container{
    clear: left;
}

#portfolio .cajaxappending .mix{-webkit-transform: translate3d(0,0,0);}
*/
</style>

<section class="page_head">
<div class="container">
<h1>	<?php _e( 'GEN CLAY<sup>TM</sup> COMMUNITY', 'twentyseventeen' ); ?> <?php //echo get_the_title($getpage->ID); ?> </h1>

	<?php echo $page_content; ?>
<div class="clearfix"></div>

</div>
</section>	
<?php $terms = get_terms( array(
    'taxonomy' => 'community-categories',
    'hide_empty' => false,
) ); 
?>
<?php /* 
<div id="portfolio">



  


		<ul class="bar">
				<li class="current"><a href="javascript:void(0)" id="all"><?php _e( 'All Results', 'twentyseventeen' ); ?> <i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
				 <?php foreach($terms as $term) { ?>			
				<li><a href="javascript:void(0)" id="<?php echo $term->slug; ?>"><?php echo $term->name; ?></a></li>
				<?php } ?>				
				 		
			</ul>


			<div class="filter-button-group tal"><button data-filter="*" class="">All</button>
                           	<button data-filter=".category-digital-art" class="active">Digital art</button>
                           	<button data-filter=".category-illustration">Illustration</button>
                           	<button data-filter=".category-photography">Photography</button>
                           	<button data-filter=".category-typography">Typography</button>
                           </div>
                           <div class="portfolio-items row portfolio-type-grid space-on portfolio_hover_type_1 portfolio-5a02e1b70ae98 popup-gallery" style="position: relative; height: 564px;">
</div>
</section>


<section class="result_sec">
	<section class="home_latest">
	<div class="container">
		<div class="row cajaxappending">

<?php $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$newsArgs = array(
		'post_type' => 'communities', 
		'post_status' => array('publish'), 
		'posts_per_page' => get_option( 'posts_per_page' ),
		'paged' => $paged
	);
	$newsQuery = new WP_Query( $newsArgs );

if ( $newsQuery->have_posts() ) :
	$mi = 1;
		while ( $newsQuery->have_posts() ) : $newsQuery->the_post();

			$image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'custom-size' );  

				$cat = wp_get_post_terms(get_the_ID(),'community-categories');


				$cat_id =  $cat[0]->term_id;

if($cat_id!=34){
	$newclass = 'home_blog';
	$newtitle = get_the_title();

	$permalink  = get_post_permalink();
} else {
	$newclass = '';

	$newtitle = get_field('community_external_url');
	$newtitle = preg_replace('#^http?://#', '', $newtitle);
	$newtitle = preg_replace('#^https?://#', '', $newtitle);
	$newtitle = preg_replace('#^www.#', '', $newtitle);

	$permalink  = get_field('community_external_url');

}
$catsalug = array();
foreach($cat as $catdata){
	$catsalug[] = $catdata->slug;
}
$implodeslug = implode(' ', $catsalug);
  ?>


			<div class="mix col-md-6 <?php echo $implodeslug; ?>" data-id="<?php echo $mi; ?>">
				<div class="block <?php echo $newclass; ?>">

					<figure>
						<img src="<?php echo $image[0]; ?>"><a href="<?php echo $permalink; ?>"></a>
					</figure>
					<div class="title"><a href="<?php echo $permalink; ?>"><?php echo $newtitle; ?></a></div>
					<div class="date"><?php echo $cat[0]->name; ?> – <?php the_date('m/d/Y'); ?></div>

				</div> 
			</div>
 	 <?php $mi++;
 	  endwhile; 

 
 	 		

			else :
			get_template_part( 'template-parts/post/content', 'none' );

		endif; 
			 ?>
		</div>

		<?php 
		if (  $newsQuery->max_num_pages > 1 ):
			 echo '<div class="community_loadmore">More Community</div>'; 
			endif;	
		?>
	</div>
</section>
	
</section>
</div>
<?php */ ?>

  <link rel="stylesheet" id="mishka-frontend-grid-css" href="<?php echo CHILD_DIR; ?>/portfolio/frontend-grid.css" media="all">
     
     
  
      <link rel="stylesheet" id="mishka-style-css" href="<?php echo CHILD_DIR; ?>/portfolio/style_002.css" media="all"> 
		
		 
		 <section class="result_sec">
		 	<section class="home_latest">
				<div class="container">
                            
                           <ul class="filter-button-group tal">

                           
				<li><a href="javascript:void(0)"  data-filter="*" class="active toggle-filter"><?php _e( 'All Results', 'twentyseventeen' ); ?> <i class="fa fa-angle-down" aria-hidden="true"></i></a> 
					 
				
				<ul class="sub-filter" style="display: none;">
				 <?php foreach($terms as $term) { ?>			
				<li><a href="javascript:void(0)" data-filter=".<?php echo $term->slug; ?>" class=""><?php echo $term->name; ?></a></li>
				<?php } ?>
				</ul>		
			 	</li>

 
                           </ul>
                           <div class="portfolio-items row portfolio-type-grid space-on portfolio_hover_type_1 portfolio-5a02e1b70ae98 popup-gallery cajaxappending" style="position: relative;">
  <?php   $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$newsArgs = array(
		'post_type' => 'communities', 
		'post_status' => array('publish'), 
		'posts_per_page' => get_option( 'posts_per_page' ),
		'paged' => $paged
	);
	$newsQuery = new WP_Query( $newsArgs );


  if ( $newsQuery->have_posts() ) :
	$mi = 1;
		while ( $newsQuery->have_posts() ) : $newsQuery->the_post();

			$image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'custom-size' );  

				$cat = wp_get_post_terms(get_the_ID(),'community-categories');


				$cat_id =  $cat[0]->term_id;

if($cat_id!=34){
	$newclass = 'home_blog';
	$newtitle = get_the_title();

	$permalink  = get_post_permalink();

	$linktarget  = '';

} else {
	$newclass = '';

	$newtitle = get_field('community_external_url');
	$newtitle = preg_replace('#^http?://#', '', $newtitle);
	$newtitle = preg_replace('#^https?://#', '', $newtitle);
	$newtitle = preg_replace('#^www.#', '', $newtitle);

	$permalink  = get_field('community_external_url');
	$linktarget  = 'target="_blank"';
}
$catsalug = array();
foreach($cat as $catdata){
	$catsalug[] = $catdata->slug;
}
$implodeslug = implode(' ', $catsalug); ?>
 
                              <article class="portfolio-item <?php echo $implodeslug; ?> col-xs-12 col-sm-6 col-md-6" style="">
                              	 <div class="block <?php echo $newclass; ?>">

									<figure>
										<img src="<?php echo $image[0]; ?>"><a href="<?php echo $permalink; ?>" <?php echo $linktarget; ?>></a>
									</figure>
									<div class="title"><a href="<?php echo $permalink; ?>" <?php echo $linktarget; ?>><?php echo $newtitle; ?></a></div>
									<div class="date"><?php echo $cat[0]->name; ?> – <?php the_date('m/d/Y'); ?></div>

								</div>
                              </article>

<?php $mi++;
 	  endwhile; 
 
			else :
			get_template_part( 'template-parts/post/content', 'none' );

		endif; 
			 ?>

   					  </div>

   					  <?php 
		if (  $newsQuery->max_num_pages > 1 ):
			 echo '<div class="community_loadmore">More Community</div>'; 
			endif;	
		?>

                          </div>
                          
                        </section> 
                        </section> 
    
 
		 
		


 
<?php get_footer();