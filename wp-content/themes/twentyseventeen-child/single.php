<?php
/**
* The template for displaying all single posts
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
*
* @package WordPress
* @subpackage Twenty_Seventeen
* @since 1.0
* @version 1.0
*/

get_header(); 
global $post;
$cid = $post->ID;
while ( have_posts() ) : the_post(); $id = get_the_ID(); ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );  ?>
<section class="main-content">
    <div class="container">
        <div class="left-content">
            <figure class="main-image"><img src="<?php echo $image[0]; ?>" /></figure>
            <div class="side-img first"><span><img src="<?php the_field('first_small_image', $id);?>" /></span></div>
            <div class="side-img second"><span><img src="<?php the_field('second_small_image', $id);?>" /></span></div>
        </div>
        <div class="right-content">
            <div class="inner-content">
                <figure><img src="<?php the_field('logo', $id);?>" /></figure>
                <h1><?php the_title(); ?></h1>
                <p><strong><?php the_field('sub_heading', $id);?></strong></p>
                <p><?php the_content(); ?></p>
            </div>
        </div>
    </div>
</section>
<section class="home-sec2">
    <div class="container">
        <h2><?php the_field('sheading', $id);?></h2>
        <p><?php the_field('ssub_heading', $id);?></p>
        <div class="producat-list">
            <ul>
                <li><figure><a href="#"><img src="<?php the_field('first_image', $id);?>" /></a></figure></li>
                <li><figure><a href="#"><img src="<?php the_field('second_image', $id);?>" /></a></figure></li>
                <li><figure><a href="#"><img src="<?php the_field('third_image', $id);?>" /></a></figure></li>
                <li><figure><a href="#"><img src="<?php the_field('fourth_image', $id);?>" /></a></figure></li>
            </ul>
        </div>
        <hr />
        <div class="more-item"> <a class="view-more" href="#">VOIR PLUS</a> </div>
    </div>
</section>
<section class="blog-sec">
    <div class="container">
        <ul class="row">
            <?php 
            $args = array(
                'posts_per_page'   => 3,
                'post_type'        => 'post',
                'post_status'      => 'publish',
                'post__not_in' => array( $post->ID )
            );
            $myposts = get_posts( $args ); ?>
            <?php foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'custom-size' );  ?>
            <li class="col-md-4">
                <div class="blog-item">
                    <h5>J. Harvest & Frost</h5>
                    <figure><a href="<?php the_permalink(); ?>"><img src="<?php echo $image[0]; ?>"/></a></figure>
                    <h3><a href="<?php the_permalink(); ?>"><?php echo substr(get_the_title(),0, 35); ?></a></h3>
                    <div class="date-item">
                        <p><?php echo get_the_date( 'M d, Y', $post->ID );?></p>
                        |
                        <p><?php the_field('reading_time', $post->ID);?></p>
                    </div>
                </div>
            </li>
            <?php endforeach; wp_reset_postdata();?>
        </ul>
        <div class="link-item"><a href="<?php bloginfo('home'); ?>">RETOURNEZ SUR LE BLOG</a></div>
    </div>
</section>
<?php endwhile; ?>  
<?php get_footer();
