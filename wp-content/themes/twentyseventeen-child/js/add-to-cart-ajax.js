jQuery( function( $ ) {


// Ajax add to cart on the product page
var $warp_fragment_refresh = {
    url: wc_cart_fragments_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'get_refreshed_fragments' ),
    type: 'POST',
    success: function( data ) {
        if ( data && data.fragments ) {

            $.each( data.fragments, function( key, value ) {
                $( key ).replaceWith( value );
            });

            $( document.body ).trigger( 'wc_fragments_refreshed' );
        }
    }
};

$('.entry-summary form.cart').on('submit', function (e)
{
    e.preventDefault();

    $('.entry-summary').block({
        message: null,
        overlayCSS: {
            cursor: 'none'
        }
    });

    var product_url = window.location;
        form = $(this);

    $.post(product_url, form.serialize() + '&_wp_http_referer=' + product_url, function (result)
    {
        var cart_dropdown = $('.widget_shopping_cart', result)
        // update dropdown cart
        $('.widget_shopping_cart').replaceWith(cart_dropdown);

        // update fragments
        $.ajax($warp_fragment_refresh);

        $.post(woocommerce_params.ajax_url, {'action': 'mini_cart_html_ajax' }, function(response) { $('.woo_mini_sidebar_cart').html(response); $('body').addClass('opencart'); });
        $.post(woocommerce_params.ajax_url, {'action': 'mini_cart_total'}, function(responsecart) { $('.cart_no').html(responsecart); $('.cart_popup h3.shopingbagtotal').html('Shopping Bag  ('+responsecart+')'); });
        $('.entry-summary').unblock();

    });
});



    $(document).on('click','.mobile-addto-cart a', function(e){
        $( "button.single_add_to_cart_button" ).trigger( "click" );
    });

    $(document).on('click','.single-product .quantity .plus', function(e){
        input = $('.single-product .quantity input.qty');
        var val = parseInt(input.val());
        var nextval = val + 1;
        input.val(nextval);
    });

    $(document).on('click','.single-product .quantity .minus', function(e){
        input = $('.single-product .quantity input.qty');
        var val = parseInt(input.val());
        var nextval = val - 1;
        if (val > 0) {
            input.val(nextval);
        } else {
            input.val(1);
        } 
    });


$(document).on('click','.mini-cart-qty .plus', function(e){
          input = $(this).closest('div.mini-cart-qty').find('input.qty');
          cartkey = $(this).closest('div.mini-cart-qty').find('input.mini-cart-key').val();
          var val = parseInt(input.val());
          var nextval = val + 1;
          input.val(nextval);           
          min_cart_update(cartkey, nextval);   
      });
$(document).on('click','.mini-cart-qty .minus', function(e){
        input = $(this).closest('div.mini-cart-qty').find('input.qty');
        cartkey = $(this).closest('div.mini-cart-qty').find('input.mini-cart-key').val();
        var val = parseInt(input.val());
        var nextval = val - 1;
        if (val > 0) {
            input.val(nextval);
        } else {
            input.val(1);
        } 
        min_cart_update(cartkey, nextval);  
    });

$(document).on('change','.mini-cart-qty input.qty', function(e){
  input = $(this).val();
  cartkey = $(this).closest('div.mini-cart-qty').find('input.mini-cart-key').val();
  min_cart_update(cartkey, input);  
}); 

function min_cart_update(cartkey, qtyval){
    $('body').block({
          message: null,
          overlayCSS: {
              cursor: 'none'
          }
        });
    $.post( woocommerce_params.ajax_url,
        {'action': 'mini_cart_qty_cart', 'hash' : cartkey, 'quantity': qtyval},
        function(response) {
            $.post(
                woocommerce_params.ajax_url,
                {'action': 'mini_cart_html_ajax'},
                function(response) {
                    $('.woo_mini_sidebar_cart').html(response);
                }
            );
            $.post(
                woocommerce_params.ajax_url,
                {'action': 'mini_cart_total'},
                function(responsecart) {
                    $('.cart_no').html(responsecart);
                    $('.cart_popup h3.shopingbagtotal').html('Shopping Bag  ('+responsecart+')');
                }
            );
        }
    );
    $('body').unblock();
}


$(document).on('click','.add_to_cart_button', function(e){
$('body').block({
          message: null,
          overlayCSS: {
              cursor: 'none'
          }
    });
});

$('body').bind('added_to_cart', function(e, fragments, cart_hash) {
    $.post(woocommerce_params.ajax_url, {'action': 'mini_cart_html_ajax' }, function(response) { $('.woo_mini_sidebar_cart').html(response); $('body').addClass('opencart'); });
    $.post(woocommerce_params.ajax_url, {'action': 'mini_cart_total'}, function(responsecart) { $('.cart_no').html(responsecart); $('.cart_popup h3.shopingbagtotal').html('Shopping Bag  ('+responsecart+')'); });
        $('body').unblock(); 
});





// Ajax delete product in the cart
$(document).on('click', '.woocommerce-mini-cart-item a.remove', function (e) {
    e.preventDefault();

    var product_id = $(this).attr("data-product_id"),
        cart_item_key = $(this).attr("data-cart_item_key"),
        product_container = $(this).parents('.mini_cart_item');

    // Add loader
    product_container.block({
        message: null,
        overlayCSS: {
            cursor: 'none'
        }
    });

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: wc_add_to_cart_params.ajax_url,
        data: {
            action: "product_remove",
            product_id: product_id,
            cart_item_key: cart_item_key
        },
        success: function(response) {
            if ( ! response || response.error )
                return;

            var fragments = response.fragments;

            // Replace fragments
            if ( fragments ) {
                $.each( fragments, function( key, value ) {
                    $( key ).replaceWith( value );
                });
                $.post(
                    woocommerce_params.ajax_url,
                    {'action': 'mini_cart_html_ajax'},
                    function(response) {
                        $('.woo_mini_sidebar_cart').html(response);
                    }
                );
                    $.post(
                        woocommerce_params.ajax_url,
                        {'action': 'mini_cart_total'},
                        function(responsecart) {
                            $('.cart_no').html(responsecart);
                            $('.cart_popup h3.shopingbagtotal').html('Shopping Bag  ('+responsecart+')');
                        }
                    );
                 $('.mini_cart_item').unblock();
            }
        }
    });
});








});



 