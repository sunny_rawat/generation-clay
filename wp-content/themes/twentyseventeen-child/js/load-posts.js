jQuery(function($){

 
			$('.portfolio-items').isotope({
				itemSelector: 'article'
			});
			$('.filter-button-group').on( 'click', 'a', function() {
				$('.filter-button-group a').removeClass('active')
				$(this).addClass('active');
				var filterValue = $(this).attr('data-filter');
				$('.portfolio-items').isotope({ filter: filterValue, itemSelector: 'article' });
				//$(window).trigger('resize').trigger('scroll');
			});
	 


	/* Blog Page Load More */
		$('.blog_loadmore').click(function(){
		var button = $(this),
		    data = {
			'action': 'blog_loadmore',
			'page' : loadingelements.current_page
		};
		$.ajax({
			url : loadingelements.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.html('Loading...'); // change the button text, you can also add a preloader image
			},
			success : function( data ){
				if( data ) {
					$(button).before(data);
					button.text( 'More Blogs' );
					//button.text( 'More Blogs' ).prev().before(data); // insert new posts
					loadingelements.current_page++;
					if ( loadingelements.current_page == loadingelements.max_page ) 
						button.remove(); // if last page, remove the button
				} else {
					button.remove(); // if no data, remove the button as well
				}
			}
		});
	});


/* Community page Load more */
$('.community_loadmore').click(function(){
		var button = $(this),
		    data = {
			'action': 'community_loadmore',
			'page' : loadingelements.current_page
		};
		$.ajax({
			url : loadingelements.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.html('Loading...'); // change the button text, you can also add a preloader image
			},
			success : function( data ){
				if( data ) {
					$('.cajaxappending').append(data); 
					button.text( 'More Community' );
					loadingelements.current_page++;
					if ( loadingelements.current_page == loadingelements.max_page ) 
						button.remove(); // if last page, remove the button
				} else {
					button.remove(); // if no data, remove the button as well
				}

				$('.portfolio-items').isotope('destroy');
				$('.portfolio-items').isotope({
										itemSelector: 'article'
									});
					 /* setTimeout(function(){ 
			 		$('.portfolio-items').each(function(){ alert('test');
				 var $grid = $('.portfolio-items').isotope({
						itemSelector: 'article'
					});

				 $(document).on('click','.filter-button-group button', function(){
					$(this).addClass('active').siblings().removeClass('active');
					var filterValue = $(this).attr('data-filter');
					$grid.isotope({ filter: filterValue });
					$(window).trigger('resize').trigger('scroll');
				}); 

				 });	
			 	}, 1000); */

				/* $('#portfolio').filterPortfolio({
					initFilter: '#all', 
					itemUL: '.cajaxappending',
					filterButtons: [ 
						{'link': '#all', 'dom': '.mix'}, 
						{'link': '#as-seen-in', 'dom': 'div.as-seen-in'}, 
						{'link': '#featured-press', 'dom': 'div.featured-press'}, 
					], 
					orderReverse: true,
					sortOption: {
						adjustHeight: 'auto',
						easeIn: 'fadeInUp',
						easeOut: 'fadeOutUp'
					}
				}); */
			}
		});
	});

/* Results page Load more */
$('.results_loadmore').click(function(){
		var button = $(this),
		    data = {
			'action': 'results_loadmore',
			'page' : loadingelements.current_page
		};
		$.ajax({
			url : loadingelements.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.html('Loading...'); // change the button text, you can also add a preloader image
			},
			success : function( data ){
				if( data ) {
					$(button).before(data);
					button.text( 'More Results' );
					loadingelements.current_page++;
					if ( loadingelements.current_page == loadingelements.max_page ) 
						button.remove(); // if last page, remove the button
				} else {
					button.remove(); // if no data, remove the button as well
				}
			}
		});
	});


});
 