<?php
$title = isset($instance['title']) ? $instance['title'] : __('Subscribe to my list', 'email-octopus');
$list_id = isset($instance['list_id']) ? $instance['list_id'] : '';
$include_referral = isset($instance['include_referral']) ? (bool)$instance['include_referral'] : true;
$success_redirect_url = isset($instance['success_redirect_url']) ? $instance['success_redirect_url'] : '';
$redirect_on_success = !empty($success_redirect_url);

$include_first_name_field = false;
$include_last_name_field = false;

if (array_key_exists('include_first_name_field', $instance)) {
    $include_first_name_field = (bool) $instance['include_first_name_field'];
}

if (array_key_exists('include_last_name_field', $instance)) {
    $include_last_name_field = (bool) $instance['include_last_name_field'];
}

settings_errors();
?>
<div class="email-octopus-widget-options">
    <p>
        <label for="<?php echo $this->get_field_id('list_id'); ?>"><?php _e('List ID:', 'email-octopus'); ?></label>
        <input id="<?php echo $this->get_field_id('list_id'); ?>" name="<?php echo $this->get_field_name('list_id'); ?>" type="text" value="<?php echo esc_attr($list_id); ?>" class="widefat">
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'email-octopus'); ?></label>
        <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" class="widefat">
    </p>
    <p>
        <input type="checkbox" <?php checked($include_first_name_field); ?> id="<?php echo $this->get_field_id('include_first_name_field'); ?>" name="<?php echo $this->get_field_name('include_first_name_field'); ?>" class="checkbox" />
        <label for="<?php echo $this->get_field_id('include_first_name_field'); ?>"><?php _e('Include optional first name field', 'email-octopus'); ?></label>
    </p>
    <p>
        <input type="checkbox" <?php checked($include_last_name_field); ?> id="<?php echo $this->get_field_id('include_last_name_field'); ?>" name="<?php echo $this->get_field_name('include_last_name_field'); ?>" class="checkbox" />
        <label for="<?php echo $this->get_field_id('include_last_name_field'); ?>"><?php _e('Include optional last name field', 'email-octopus'); ?></label>
    </p>
    <p>
        <input type="checkbox" <?php checked($redirect_on_success); ?> id="<?php echo $this->get_field_id('redirect_on_success'); ?>" name="<?php echo $this->get_field_name('redirect_on_success'); ?>" class="checkbox redirect-on-success" />
        <label for="<?php echo $this->get_field_id('redirect_on_success'); ?>"><?php _e('Instead of thanking the user for subscribing, redirect them to a URL', 'email-octopus'); ?></label>
    </p>
    <p class="success-redirect-url-wrapper">
        <input id="<?php echo $this->get_field_id('success_redirect_url'); ?>" name="<?php echo $this->get_field_name('success_redirect_url'); ?>" type="text" value="<?php echo esc_attr($success_redirect_url); ?>" placeholder="URL" class="widefat success-redirect-url">
    </p>
</div>
